const algoliasearch = require('algoliasearch')

const searchAirlines = async (appId, adminApiKey, airlinesIndex, query) => {
  try {
    const client = algoliasearch(appId, adminApiKey)
    const index = client.initIndex(airlinesIndex)
    const search = await index.search({
      query
    })
    return search.hits.map((item) => {
      return {
        name: item.name,
        iata_code: item.iata_code,
        objectID: item.objectID
      }
    })
  } catch (err) {
    console.log(err)
    return err
  }
}

module.exports = {
  searchAirlines
}
