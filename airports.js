const algoliasearch = require('algoliasearch')
const _ = require('lodash')
const { partialUpdateObjects, getObjects } = require('./utils')
const { getCityById } = require('./cities')

const filterResults = (results) => {
  let matchedAirports = false
  results._highlightResult.airports.forEach((item) => {
    if (item.matchLevel !== 'none') {
      matchedAirports = true
    }
  })
  if (!matchedAirports && (results._highlightResult.city_name.matchLevel !== 'none' || results._highlightResult.iata_city_code.matchLevel !== 'none' || results._highlightResult.foreign_names.matchLevel !== 'none')) {
    return {
      city: results.city_name,
      flag: results.flag,
      country: results.country,
      airports: results.children,
      objectID: results.objectID
    }
  }
  const airports = results._highlightResult.airports
  let searchResults = []
  airports.forEach((item, i) => {
    if (item.matchLevel !== 'none') {
      searchResults.push(results.airports[i])
    }
  })
  const children = results._highlightResult.children
  children.forEach((item, i) => {
    if (item.name.matchLevel !== 'none' && !searchResults.includes(airports[i])) {
      searchResults.push(airports[i].value)
    }
  })
  return {
    city: results.city_name,
    flag: results.flag,
    country: results.country,
    airports: results.children.filter(item => searchResults.includes(item.iata_code)),
    objectID: results.objectID
  }
}

const getNearbyAirports = async (index, airports) => {
  try {
    const results = []
    let i = 0
    while (i < airports.length) {
      const coordinates = await index.search({
        query: airports[i],
        restrictSearchableAttributes: [
          'airports'
        ],
        attributesToRetrieve: [
          '_geoloc'
        ]
      })
      if (coordinates.nbHits > 0) {
        const nearbyAirports = await index.search({
          query: '',
          filters: `NOT airports:${airports[i]}`,
          aroundLatLng: `${coordinates.hits[0]._geoloc.lat}, ${coordinates.hits[0]._geoloc.lng}`,
          aroundRadius: 50 * 1000,
          getRankingInfo: true,
          attributesToRetrieve: [
            'city_name',
            'children'
          ]
        })
        results.push(...nearbyAirports.hits.map((item) => {
          return {
            city: item.city_name,
            flag: item.flag,
            country: item.country,
            airports: item.children,
            nearbyFrom: airports[i],
            distance: item._rankingInfo.matchedGeoLocation.distance,
            objectID: item.objectID
          }
        }))
      }
      i++
    }
    return results
  } catch (err) {
    return err
  }
}

// perform a textual search on the airport index. For now, only one option is available. Set nearby === true if you want nearbyAirports (up to a distance of 50km) to be considered
const searchAirports = async (appId, adminApiKey, airportsIndex, query, options) => {
  try {
    const client = algoliasearch(appId, adminApiKey)
    const index = client.initIndex(airportsIndex)
    const search = await index.search({
      query
    })
    if (query.length === 0) {
      return search.hits.map((item) => {
        return {
          city: item.city_name,
          flag: item.flag,
          country: item.country,
          airports: item.children,
          objectID: item.objectID
        }
      })
    }
    const results = search.hits.map((item) => {
      return filterResults(item)
    })
    if (options && options.nearby) {
      const topResults = results.slice(0, 4)
      const promiseArray = topResults.map((item) => {
        return getNearbyAirports(index, item.airports.map(elem => elem.iata_code))
      })
      const nearbyAirports = await Promise.all(promiseArray)

      nearbyAirports.forEach((item) => {
        if (item.length > 0) {
          topResults.push(...item)
        }
      })
      return topResults
    } else {
      return results
    }
  } catch (err) {
    console.log(err)
    return err
  }
}

const getAirport = async (appId, adminApiKey, airportsIndex, airportID) => {
  try {
    const client = algoliasearch(appId, adminApiKey)
    const index = client.initIndex(airportsIndex)
    let result = []
    const search = await index.search({
      query: airportID,
      restrictSearchableAttributes: ['children.airportID'],
      attributesToHighlight: ['children.airportID'],
      typoTolerance: false,
      getRankingInfo: true
    })

    const filterResults = search.hits.filter((item) => {
      return item._rankingInfo.nbExactWords === 1
    })

    if (filterResults.length > 1) {
      throw new Error('there is more than one airport with this id. There must be a problem, check the index.')
    }
    filterResults[0]._highlightResult.children.forEach((item, i) => {
      if (item.airportID.matchLevel === 'full') {
        result.push({
          city: filterResults[0].city_name,
          flag: filterResults[0].flag,
          country: filterResults[0].country,
          objectID: filterResults[0].objectID,
          airports: [filterResults[0].children[i]]
        })
      }
    })
    return result
  } catch (err) {
    console.log(err)
    return err
  }
}

const partialUpdateAirport = async (appId, adminApiKey, airportsIndex, airportID, attributesToChange) => {
  try {
    const oldAirport = await getAirport(appId, adminApiKey, airportsIndex, airportID)
    const object = await getObjects(appId, adminApiKey, airportsIndex, [oldAirport[0].objectID])
    const newAirport = {
      ..._.omit(oldAirport[0].airports[0], Object.keys(attributesToChange)),
      ...attributesToChange
    }
    const children = [
      newAirport,
      ...object.results[0].children.filter((elem) => elem.airportID !== airportID)
    ]
    await partialUpdateObjects(appId, adminApiKey, airportsIndex, [{ objectID: oldAirport[0].objectID, children }])
    return 'success'
  } catch (err) {
    console.log(err)
    return err
  }
}

// To add an airport to the algolia index, supply the airport information along with the associated city information.

const addAirport = async (appId, adminApiKey, airportsIndex, cityId, airport) => {
  try {
    if (!airport.name) {
      throw new Error('missing airport name')
    }
    if (!airport.iata_code) {
      throw new Error('missing airport iata_code')
    }
    if (!airport.timezone) {
      throw new Error('missing airport timezone')
    }

    const client = algoliasearch(appId, adminApiKey)
    const index = client.initIndex(airportsIndex)
    let hits = []
    let topAirportID = 0
    const browser = index.browseAll()

    browser.on('result', (content) => {
      hits = hits.concat(content.hits)
    })

    const city = await getCityById(appId, adminApiKey, airportsIndex, cityId)

    browser.on('end', () => {
      hits.forEach((item) => {
        if (item.children) {
          item.children.forEach((elem) => {
            if (parseInt(elem.airportID) > topAirportID) {
              topAirportID = parseInt(elem.airportID)
            }
          })
        }
      })
      const newChild = {
        airportID: (topAirportID + 1).toString(),
        ...airport
      }
      const insertObject = {
        airports: city.airports ? [airport.iata_code, ...city.airports] : [airport.iata_code],
        children: city.children ? [newChild, ...city.children] : [newChild],
        ..._.omit(city, ['airports', 'children'])
      }
      index.saveObject(insertObject, (err, content) => {
        if (err) {
          throw err
        }
        console.log('index updated')
        return 'index updated'
      })
    })
  } catch (err) {
    console.log(err)
    return err
  }
}

module.exports = {
  searchAirports,
  addAirport,
  partialUpdateAirport,
  getAirport
}
