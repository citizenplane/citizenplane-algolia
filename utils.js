const algoliasearch = require('algoliasearch')

// In order to partially update an object, an array of objects has to be passed as an arguments. Each object must contain an objectID and the attributes you want to update. If you supply an unknown objectId, a new record with the supplied objectID and attributes will be created. If you wish to avoid this, set createNew as false (defaults to true)

const partialUpdateObjects = (appId, adminApiKey, indexName, objects, createNew) => {
  const createIfNotExists = createNew !== false
  const client = algoliasearch(appId, adminApiKey)
  const index = client.initIndex(indexName)
  return new Promise((resolve, reject) => {
    index.partialUpdateObjects(objects, createIfNotExists, (err, content) => {
      if (err) {
        reject(err)
      } else {
        resolve(content)
      }
    })
  })
}

// Retrieve one or more objects by supplying an array of objectIds

const getObjects = (appId, adminApiKey, indexName, objectIdsArray) => {
  const client = algoliasearch(appId, adminApiKey)
  const index = client.initIndex(indexName)
  return new Promise((resolve, reject) => {
    index.getObjects(objectIdsArray, (err, content) => {
      if (err) {
        reject(err)
      } else {
        resolve(content)
      }
    })
  })
}

// Insert one or more objects into a specified index. Make sure to supply all the needed attributes. If you don't supply an objectID, a random one will be assigned.

const addObjects = (appId, adminApiKey, indexName, objects) => {
  const client = algoliasearch(appId, adminApiKey)
  const index = client.initIndex(indexName)
  return new Promise((resolve, reject) => {
    index.addObjects(objects, (err, content) => {
      if (err) {
        reject(err)
      } else {
        resolve(content)
      }
    })
  })
}

module.exports = {
  partialUpdateObjects,
  getObjects,
  addObjects
}
