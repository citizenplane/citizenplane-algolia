'use strict'

const { searchAirlines } = require('./airlines')
const { searchAirports, getAirport, partialUpdateAirport, addAirport } = require('./airports')
const { exportCities, searchCity, addCity } = require('./cities')
const { partialUpdateObjects, getObjects, addObjects } = require('./utils.js')

class CitizenPlaneAlgolia {
  constructor ({appId, adminApiKey, airlinesIndex, airportsIndex}) {
    this.appId = appId
    this.adminApiKey = adminApiKey
    this.airlinesIndex = airlinesIndex
    this.airportsIndex = airportsIndex
  }

  // Performs a textual search on the airlines index and returns an array of sorted results.
  searchAirlines (query) {
    return searchAirlines(this.appId, this.adminApiKey, this.airlinesIndex, query)
  }

  // Performs a textual search on the airport index. For now, only one option is available. Set nearby === true if you want nearbyAirports (up to a distance of 50km) to be considered. Results will be filtered by cities.
  searchAirports (query) {
    return searchAirports(this.appId, this.adminApiKey, this.airportsIndex, query)
  }

  // To retrieve all the information associated to an airport, pass the associated airport ID as an argument to this function. An object containing all the airport attributes will be returned.
  getAirport (airportID) {
    return getAirport(this.appId, this.adminApiKey, this.airportsIndex, airportID)
  }

  // To change one or more attributes belonging to one airport, pass the associated airportID along with the attributes you wish to change as arguments. New attributes can be added to the airport object as well.
  partialUpdateAirport (airportID, attributesToChange) {
    return partialUpdateAirport(this.appId, this.adminApiKey, this.airportsIndex, airportID, attributesToChange)
  }

  // To add an airport to the algolia index, supply the airport information along with the associated city information. Make sure to provide at least the following attributes: name, iata_code and timezone. Optional information: icao_code, _geoloc (passed as an object containing two attributes: lat and lng), foreign_names, GMT and terminals
  addAirport (airport) {
    return addAirport(this.appId, this.adminApiKey, this.airportsIndex, airport)
  }

  // Export all the cities in the airport index. Only city_name and objectID will be returned for each city.
  exportCities () {
    return exportCities(this.appId, this.adminApiKey, this.airportsIndex)
  }

  // Performs a textual search on the airport index. A simple object containing an array of results containing both the city_name and the objectID will be returned.
  searchCity (query) {
    return searchCity(this.appId, this.adminApiKey, this.airportsIndex, query)
  }

  // Add a new city in the airport index. In case you want to add an airport that is not affiliated with any city in the index, please ass a city in the index before adding the airport thanks to the objectID that will be returned by this call. The required field are: city_name, iata_city_code, flag and _geoloc (object containing two attributes: lat and lng). Returns a taskId and an objectID for the newly created object.
  addCity (city) {
    return addCity(this.appId, this.adminApiKey, this.airportsIndex, city)
  }

  // Leverage the algolia method partialUpdateObjects. Pass an array of objects with the objectIDs and the attributes you wish to update and they will be updated. If you want to allow object creation when no matching objectID is found, set createNew to true (defaults to false).
  partialUpdateObjects (objects, createNew, indexName) {
    return partialUpdateObjects(this.appId, this.adminApiKey, indexName, objects, createNew)
  }

  // Leverage the Algolia method getObjects  Pass an array of objectIDs and corresponding objects will be returned in an array.
  getObjects (objectIds, indexName) {
    return getObjects(this.appId, this.adminApiKey, indexName, objectIds)
  }

  // Leverage the Algolia method addObjects. Pass an object with the attributes you want to add and the indexName.
  addObjects (objects, indexName) {
    return addObjects(this.appId, this.adminApiKey, indexName, objects)
  }
}

module.exports = CitizenPlaneAlgolia
