const algoliasearch = require('algoliasearch')

const exportCities = (appId, adminApiKey, airportsIndex) => {
  const client = algoliasearch(appId, adminApiKey)
  const index = client.initIndex(airportsIndex)
  let hits = []
  const browser = index.browseAll()

  browser.on('result', (content) => {
    hits = hits.concat(content.hits)
  })

  browser.on('end', () => {
    return hits.map((item) => {
      return {
        objectID: hits.objectID,
        city_name: hits.city_name
      }
    })
  })
}

const searchCity = async (appId, adminApiKey, airportsIndex, query) => {
  try {
    const client = algoliasearch(appId, adminApiKey)
    const index = client.initIndex(airportsIndex)
    const search = await index.search({
      query,
      restrictSearchableAttributes: ['city_name'],
      attributesToHighlight: ['city_name'],
      getRankingInfo: true
    })

    return search.hits.map((item) => {
      return {
        city_name: item.city_name,
        objectID: item.objectID
      }
    })
  } catch (err) {
    console.log(err)
    return err
  }
}

const getCityById = (appId, adminApiKey, airportsIndex, objectID) => {
  const client = algoliasearch(appId, adminApiKey)
  const index = client.initIndex(airportsIndex)
  return new Promise((resolve, reject) => {
    index.getObjects([objectID], (err, content) => {
      if (err) {
        reject(err)
      }
      resolve(content.results[0])
    })
  })
}

const addCity = (appId, adminApiKey, airportsIndex, city) => {
  const client = algoliasearch(appId, adminApiKey)
  const index = client.initIndex(airportsIndex)

  if (!city.city_name) {
    throw new Error('missing city name')
  }
  if (!city.iata_city_code) {
    throw new Error('missing iata_city_code')
  }
  if (!city.country) {
    throw new Error('missing country')
  }
  if (!city.flag) {
    throw new Error('missing flag')
  }
  if (!city._geoloc) {
    throw new Error('missing _geoloc object')
  }

  return new Promise((resolve, reject) => {
    index.addObject(city, (err, content) => {
      if (err) {
        reject(err)
      }
      resolve(content)
    })
  })
}

module.exports = {
  exportCities,
  searchCity,
  getCityById,
  addCity
}
